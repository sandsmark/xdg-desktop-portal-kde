/*
 * Copyright © 2016-2018 Red Hat, Inc
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *       Jan Grulich <jgrulich@redhat.com>
 */

#include "appchooser.h"
#include "utils.h"

#include <KOpenWithDialog>

#include <QLoggingCategory>


Q_LOGGING_CATEGORY(XdgDesktopPortalKdeAppChooser, "xdp-kde-app-chooser")

AppChooserPortal::AppChooserPortal(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
}

AppChooserPortal::~AppChooserPortal()
{
}

uint AppChooserPortal::ChooseApplication(const QDBusObjectPath &handle,
                                         const QString &app_id,
                                         const QString &parent_window,
                                         const QStringList &choices,
                                         const QVariantMap &options,
                                         QVariantMap &results)
{
    qCDebug(XdgDesktopPortalKdeAppChooser) << "ChooseApplication called with parameters:";
    qCDebug(XdgDesktopPortalKdeAppChooser) << "    handle: " << handle.path();
    qCDebug(XdgDesktopPortalKdeAppChooser) << "    app_id: " << app_id;
    qCDebug(XdgDesktopPortalKdeAppChooser) << "    parent_window: " << parent_window;
    qCDebug(XdgDesktopPortalKdeAppChooser) << "    choices: " << choices;
    qCDebug(XdgDesktopPortalKdeAppChooser) << "    options: " << options;

    const QString mimeType = options.value(QStringLiteral("content_type")).toString();
    const QString fileName = options.value(QStringLiteral("filename")).toString();
    const QString lastChoice = options.value(QStringLiteral("last_choice")).toString();

    QPointer<KOpenWithDialog> dialog = new KOpenWithDialog(
            { QUrl::fromLocalFile(fileName) }, // the spec doesn't give a whole path... we at least get the filename
            mimeType,
            {},
            lastChoice);
    dialog->hideNoCloseOnExit();
    dialog->hideRunInTerminal();

    int result = dialog->exec();

    if (!dialog) { // In case we quit for some other reason it might have been deleted already
        return 0;
    }
    dialog->deleteLater();

    if (!result || !dialog->service()) {
        return 0;
    }
    results.insert(QStringLiteral("choice"), dialog->service()->entryPath());
    return result;
}

void AppChooserPortal::UpdateChoices(const QDBusObjectPath &handle, const QStringList &choices)
{
    qCDebug(XdgDesktopPortalKdeAppChooser) << "UpdateChoices called with parameters:";
    qCDebug(XdgDesktopPortalKdeAppChooser) << "    handle: " << handle.path();
    qCDebug(XdgDesktopPortalKdeAppChooser) << "    choices: " << choices;

    // this API seems designed around Gnome's limited APIs
}
